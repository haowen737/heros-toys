import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

const Home = React.lazy(() => import('./routes/home'))
const Child = React.lazy(() => import('./routes/child'))

import './index.css'

function App() {
  return (
    <div className="app">
      hello world
      <Link to="/home">to home</Link>
      <Link to="/child">to child</Link>
    </div>
  )
}

ReactDOM.render(
  <Router>
    <React.Suspense fallback={<div>Loading...</div>}>
      <Route path="/" exact component={App}></Route>
      <Route path="/home" exact component={Home} />
      <Route path="/child" exact component={Child} />
    </React.Suspense>
  </Router>
, document.querySelector('#app'))
