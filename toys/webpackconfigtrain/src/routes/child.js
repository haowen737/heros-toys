import React from 'react'
import './child.css'

export default function Child() {
  return (
    <div className="child">child</div>
  )
}