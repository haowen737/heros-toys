const Koa = require('koa')
const EventEmitter = require('events');
const myEmitter = new EventEmitter();
const app = new Koa()
app.use(async (ctx, next) => {
  console.log(ctx);
  await next()
});
app.use(require('koa-static')('./dist'))
app.on('error', (e) => {
  console.log(e)
})
app.listen(8888)