const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackBar = require('webpackbar')
const WebpackCleanupPlugin = require('webpack-cleanup-plugin')
const eloader = require('./eloader')
const path = require('path')

module.exports = {
  entry: './src/index.js',
  mode: 'development',
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }, {
          loader: path.resolve(__dirname, './eloader.js')
        }]
      }, {
        test: /\.css$/i,
        use: [{
          loader: MiniCssExtractPlugin.loader,
          options: {
            // you can specify a publicPath here
            // by default it uses publicPath in webpackOptions.output
            // publicPath: '../dist',
            // hmr: process.env.NODE_ENV === 'development',
          },
        }, {
          loader: 'css-loader'
        }]
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin({
      banner: 'Made by Haowen'
    }),
    new WebpackBar(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // all options are optional
      filename: '[name].css',
      chunkFilename: '[id].css',
      ignoreOrder: false, // Enable to remove warnings about conflicting order
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),
    new WebpackCleanupPlugin(),
  ],
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  }
}
